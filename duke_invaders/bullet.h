// bullet.h: interface for the bullet class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BULLET_H__C492045A_7544_46DE_AE85_9021898DF18B__INCLUDED_)
#define AFX_BULLET_H__C492045A_7544_46DE_AE85_9021898DF18B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "allegro.h"
extern BITMAP* buffer;

class bullet  
{
public:
	bullet();
	void setpos(int _x, int _y, int _vy)
	{
		x = _x;
		y = _y;
		vy = _vy;

		active = true;
	}
	virtual ~bullet();

	void draw();

	void destroy() { active = false; }

	bool isactive() { return active; }
	int getdir() { return vy; }
	int getx() { return x; }
	int gety() { return y; }
private:
	int x;
	int y;
	int vy;
	bool active;

};

#endif // !defined(AFX_BULLET_H__C492045A_7544_46DE_AE85_9021898DF18B__INCLUDED_)
